import { TestBed } from "@angular/core/testing";
import { CountryComponent } from './country.component';
import { CountryService } from '../service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { PreloaderModule } from 'src/app/core/preloader';
import { take } from 'rxjs/operators';

describe('CountryComponent', () => {

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule, PreloaderModule],
            declarations: [CountryComponent],
            providers: [CountryService]
        }).compileComponents();
    });

    it('Should create component', () => {
        const fixture = TestBed.createComponent(CountryComponent),
            component = fixture.debugElement.componentInstance;

        expect(component).toBeTruthy();
    });

    it('Should search country correctly', () => {
        const fixture = TestBed.createComponent(CountryComponent),
            component: CountryComponent = fixture.debugElement.componentInstance;

            component.masterCountryList = [{
                "name": "Afghanistan",
                "topLevelDomain": [
                    ".af"
                ],
                "alpha2Code": "AF",
                "alpha3Code": "AFG",
                "callingCodes": [
                    "93"
                ],
                "capital": "Kabul",
                "altSpellings": [
                    "AF",
                    "Afġānistān"
                ],
                "region": "Asia",
                "subregion": "Southern Asia",
                "population": 27657145,
                "latlng": [
                    33.0,
                    65.0
                ],
                "demonym": "Afghan",
                "area": 652230.0,
                "gini": 27.8,
                "timezones": [
                    "UTC+04:30"
                ],
                "borders": [
                    "IRN",
                    "PAK",
                    "TKM",
                    "UZB",
                    "TJK",
                    "CHN"
                ],
                "nativeName": "افغانستان",
                "numericCode": "004",
                "currencies": [
                    {
                        "code": "AFN",
                        "name": "Afghan afghani",
                        "symbol": "؋"
                    }
                ],
                "languages": [
                    {
                        "iso639_1": "ps",
                        "iso639_2": "pus",
                        "name": "Pashto",
                        "nativeName": "پښتو"
                    },
                    {
                        "iso639_1": "uz",
                        "iso639_2": "uzb",
                        "name": "Uzbek",
                        "nativeName": "Oʻzbek"
                    },
                    {
                        "iso639_1": "tk",
                        "iso639_2": "tuk",
                        "name": "Turkmen",
                        "nativeName": "Türkmen"
                    }
                ],
                "translations": {
                    "de": "Afghanistan",
                    "es": "Afganistán",
                    "fr": "Afghanistan",
                    "ja": "アフガニスタン",
                    "it": "Afghanistan",
                    "br": "Afeganistão",
                    "pt": "Afeganistão",
                    "nl": "Afghanistan",
                    "hr": "Afganistan",
                    "fa": "افغانستان"
                },
                "flag": "https://restcountries.eu/data/afg.svg",
                "regionalBlocs": [
                    {
                        "acronym": "SAARC",
                        "name": "South Asian Association for Regional Cooperation",
                        "otherAcronyms": [],
                        "otherNames": []
                    }
                ],
                "cioc": "AFG"
            }];
            component.countryList$.next(component.masterCountryList);
            component.countryList$.pipe(take(1)).subscribe(countryList => {
                expect(countryList.length).toEqual(1);
            });

            component.onSearch('Thai');
            component.countryList$.pipe(take(1)).subscribe(countryList => {
                expect(countryList.length).toEqual(0);
            });
    });

});