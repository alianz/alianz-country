import { Component, ViewEncapsulation, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { CountryService } from '../service';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { ICountry } from 'src/app/shared/models';
import { take } from 'rxjs/operators';

@Component({
    selector: 'app-country',
    templateUrl: './country.component.html',
    styleUrls: ['./country.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CountryComponent implements OnInit {

    countryList$ = new BehaviorSubject<ICountry[]>([]);
    compDestroy$ = new Subject();

    masterCountryList: ICountry[];

    constructor(private countryService: CountryService) { }

    ngOnInit() {
        this.countryService.fetchAllCountry().pipe(take(1))
            .subscribe(countryList => {
                this.masterCountryList = countryList;
                this.countryList$.next(countryList);
            })
    }

    onSearch(keyword: string) {
        if (keyword) {
            const filteredCountryList = this.masterCountryList
                .filter(country => country.name.toLowerCase().includes(keyword.toLowerCase()));
                
            this.countryList$.next(filteredCountryList);
        } else {
            this.countryList$.next(this.masterCountryList);
        }
    }

}