import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';

import * as fromContainer from './container';
import * as fromComponent from './component';
import * as fromService from './service';
import { SharedModule } from 'src/app/shared';

@NgModule({
    imports: [
        CommonModule, 
        HttpClientModule,
        SharedModule
    ],
    declarations: [
        ...fromContainer.container, 
        ...fromComponent.components
    ],
    exports: [
        ...fromContainer.container, 
        ...fromComponent.components
    ],
    providers: [...fromService.service]
})
export class CountryModule {}