import { Component, ViewEncapsulation, ChangeDetectionStrategy, Input } from '@angular/core';
import { ICountry } from 'src/app/shared/models';

@Component({
    selector: 'app-country-table',
    templateUrl: './country-table.component.html',
    styleUrls: ['./country-table.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CountryTableComponent {

    @Input() countryList: ICountry[];

}