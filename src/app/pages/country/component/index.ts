import { CountryTableComponent } from './country-table';
import { CountrySearchComponent } from './country-search';

export const components = [CountryTableComponent, CountrySearchComponent];

export * from './country-table';
export * from './country-search';