import { TestBed } from "@angular/core/testing";
import { CountrySearchComponent } from './country-search.component';

describe('CountrySearchComponent', () => {


    beforeEach(async () => {
        TestBed.configureTestingModule({
            declarations: [CountrySearchComponent]
        }).compileComponents();
    });

    it('Should create the country search app', () => {
        const fixture = TestBed.createComponent(CountrySearchComponent),
            countrySearchComp = fixture.debugElement.componentInstance;
        expect(countrySearchComp).toBeTruthy();
    });

    it('Should reset search keyword', () => {
        const fixture = TestBed.createComponent(CountrySearchComponent),
            countrySearchComp = fixture.debugElement.componentInstance;
        countrySearchComp.searchKeywordCtrl.setValue('Something ..');
        countrySearchComp.doClearSearchKeyword();
        expect(countrySearchComp.searchKeywordCtrl.value).toEqual('');
    });

    it('Should emit search keyword', () => {
        const fixture = TestBed.createComponent(CountrySearchComponent),
            countrySearchComp: CountrySearchComponent = fixture.debugElement.componentInstance;

        spyOn(countrySearchComp.doSearch, 'emit');

        countrySearchComp.doSearch.emit('a');

        expect(countrySearchComp.doSearch.emit).toHaveBeenCalledWith('a');
    });

});