import { Component, Output, EventEmitter, ViewEncapsulation, ChangeDetectionStrategy, OnInit, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
    selector: 'app-country-search',
    templateUrl: './country-search.component.html',
    styleUrls: ['./country-search.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CountrySearchComponent implements OnInit, OnDestroy {

    @Output() doSearch = new EventEmitter<string>();

    private compDestroy$ = new Subject();

    searchKeywordCtrl = new FormControl('');

    ngOnInit() {
        this.searchKeywordCtrl.valueChanges
            .pipe(takeUntil(this.compDestroy$))
            .subscribe(val => this.doSearch.emit(val));
    }

    ngOnDestroy() {
        this.compDestroy$.next();
        this.compDestroy$.complete();
    }

    doClearSearchKeyword() {
        this.searchKeywordCtrl.reset('');
    }

    get searchKeywordValue$(): Observable<string> {
        return this.searchKeywordCtrl.valueChanges;
    }

}