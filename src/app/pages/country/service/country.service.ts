import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ICountry } from './../../../shared/models';
import { Observable } from 'rxjs';
import { PreloaderService } from 'src/app/core/preloader';
import { tap } from 'rxjs/operators';

@Injectable()
export class CountryService {

    private readonly endpoint = 'https://restcountries.eu/rest/v2/all';

    constructor(private http: HttpClient, private preloaderService: PreloaderService) {}

    public fetchAllCountry(): Observable<ICountry[]> {
        this.preloaderService.startLoading();
        return this.http.get<ICountry[]>(this.endpoint)
            .pipe(tap({
                next: () => this.preloaderService.stopLoading(),
                error: err => {
                    console.error(err);
                }
            }));
    }

}