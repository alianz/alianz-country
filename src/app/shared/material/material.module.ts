import { NgModule } from '@angular/core';

import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

const useModules = [
    MatCardModule, 
    MatInputModule,
    MatButtonModule,
    MatIconModule
];

@NgModule({
    imports: useModules,
    exports: useModules
})
export class MaterialModule {}