import { NgModule } from '@angular/core';

import { MaterialModule } from './material';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [MaterialModule, ReactiveFormsModule],
    exports: [MaterialModule, ReactiveFormsModule]
})
export class SharedModule {}