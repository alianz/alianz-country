import { NgModule } from '@angular/core';

import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import * as fromContainer from './container';
import * as fromService from './service';
import { CommonModule } from '@angular/common';

@NgModule({
    imports: [CommonModule, MatProgressSpinnerModule],
    declarations: [...fromContainer.container],
    exports: [...fromContainer.container],
    providers: [...fromService.service]
})
export class PreloaderModule {}