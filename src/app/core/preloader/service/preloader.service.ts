import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class PreloaderService {

    loading = new BehaviorSubject<boolean>(false);

    startLoading(): void {
        this.loading.next(true);
    }

    stopLoading(): void {
        this.loading.next(false);
    }

}