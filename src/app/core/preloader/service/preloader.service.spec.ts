import { TestBed, getTestBed } from "@angular/core/testing";
import { PreloaderService } from './preloader.service';
import { take } from 'rxjs/operators';

describe('PreloaderService', () => {

    let injector: TestBed,
        service: PreloaderService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [PreloaderService]
        });

        injector = getTestBed();
        service = injector.get(PreloaderService);
    });

    it('Should set loading to true', () => {
        service.startLoading();
        service.loading.pipe(take(1)).subscribe(isLoading => {
            expect(isLoading).toEqual(true);
        });
    });

    it('Should set loading to false', () => {
        service.stopLoading();
        service.loading.pipe(take(1)).subscribe(isLoading => {
            expect(isLoading).toEqual(false);
        });
    });

});