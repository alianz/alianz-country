import { Component, ViewEncapsulation, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { PreloaderService } from '../service';
import { BehaviorSubject } from 'rxjs';

@Component({
    selector: 'app-preloader',
    templateUrl: './preloader.component.html',
    styleUrls: ['./preloader.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PreloaderComponent implements OnInit {

    isLoading$: BehaviorSubject<boolean>;

    constructor(private preloaderService: PreloaderService) {}

    ngOnInit() {
        this.isLoading$ = this.preloaderService.loading;
    }

}