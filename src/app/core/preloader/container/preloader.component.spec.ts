import { TestBed, ComponentFixture, getTestBed } from "@angular/core/testing"
import { PreloaderService } from '../service'
import { PreloaderComponent } from './preloader.component'
import { BehaviorSubject } from 'rxjs';

describe('PreloaderComponent', () => {
    
    let fixture: ComponentFixture<PreloaderComponent>,
        injector: TestBed,
        component: PreloaderComponent,
        service: PreloaderService;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            declarations: [PreloaderComponent],
            providers: [PreloaderService]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(PreloaderComponent);
        injector = getTestBed();
        component = fixture.debugElement.componentInstance;
        service = injector.get(PreloaderService);
        fixture.detectChanges();
    });

    it('Should to create component', () => {
        expect(component).toBeTruthy();
    });

    it('Should to display preload', () => {
        const nativeElement = fixture.debugElement.nativeElement
        service.startLoading();

        fixture.detectChanges();

        const preloadElement = nativeElement.querySelector('mat-spinner');
        expect(preloadElement).toBeTruthy();
    });

    it('Should to hide preload', () => {
        const nativeElement = fixture.debugElement.nativeElement
        service.stopLoading();

        fixture.detectChanges();

        const preloadElement = nativeElement.querySelector('mat-spinner');
        expect(preloadElement).toBeFalsy();
    });

})