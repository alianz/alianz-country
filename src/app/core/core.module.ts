import { NgModule } from '@angular/core';

import { HeaderModule } from './header';
import { PreloaderModule } from './preloader';

@NgModule({
    imports: [HeaderModule, PreloaderModule],
    exports: [HeaderModule, PreloaderModule]
})
export class CoreModule {}