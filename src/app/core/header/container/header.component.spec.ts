import { TestBed } from "@angular/core/testing";
import { HeaderComponent } from './header.component';

describe('HeaderComponent', () => {

    beforeEach(async () => {
        TestBed.configureTestingModule({
            declarations: [HeaderComponent]
        }).compileComponents();
    });

    it('Should create component', () => {
        const fixture = TestBed.createComponent(HeaderComponent),
            component = fixture.debugElement.componentInstance;

        expect(component).toBeTruthy();
    });

});