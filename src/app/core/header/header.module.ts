import { NgModule } from '@angular/core';
import { MatToolbarModule } from '@angular/material/toolbar';

import * as fromContainer from './container';

@NgModule({
    imports: [MatToolbarModule],
    declarations: [...fromContainer.containers],
    exports: [...fromContainer.containers]
})
export class HeaderModule {}